-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 15, 2018 at 10:24 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app_novika`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_arsip`
--

CREATE TABLE IF NOT EXISTS `tbl_arsip` (
  `id_arsip` int(5) NOT NULL AUTO_INCREMENT,
  `id_jurnal` int(5) NOT NULL,
  `volume` varchar(80) NOT NULL,
  `tahun` int(4) NOT NULL,
  PRIMARY KEY (`id_arsip`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_arsip`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_jurnal`
--

CREATE TABLE IF NOT EXISTS `tbl_jurnal` (
  `id_jurnal` int(5) NOT NULL AUTO_INCREMENT,
  `id_user` int(5) NOT NULL,
  `judul_jurnal` varchar(125) NOT NULL,
  `abstrak` text NOT NULL,
  `deskripsi` text NOT NULL,
  `file` text NOT NULL,
  `gambar` varchar(40) NOT NULL,
  `kata_kunci` text NOT NULL,
  `referensi` text NOT NULL,
  `tanggal_publikasi` date NOT NULL,
  `tahun` int(5) NOT NULL,
  `status_revisi` tinyint(4) NOT NULL DEFAULT '0',
  `status_terbit` tinyint(5) NOT NULL DEFAULT '0',
  `persetujuan` tinyint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_jurnal`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tbl_jurnal`
--

INSERT INTO `tbl_jurnal` (`id_jurnal`, `id_user`, `judul_jurnal`, `abstrak`, `deskripsi`, `file`, `gambar`, `kata_kunci`, `referensi`, `tanggal_publikasi`, `tahun`, `status_revisi`, `status_terbit`, `persetujuan`) VALUES
(15, 6, 'Coba', 'asd', 'asd', '1539612039.pdf', '1539612039.jpg', 'asdasdasd', 'asd', '2018-10-15', 2018, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_revisi`
--

CREATE TABLE IF NOT EXISTS `tbl_revisi` (
  `id_revisi` int(5) NOT NULL AUTO_INCREMENT,
  `id_jurnal` int(5) NOT NULL,
  `id_user` int(5) NOT NULL,
  `revisi` text NOT NULL,
  `tanggal_revisi` date NOT NULL,
  PRIMARY KEY (`id_revisi`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_revisi`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nama_pengguna` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` tinyint(5) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username`, `password`, `nama_pengguna`, `email`, `status`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'admin@localhost', 1),
(3, 'editor', '5aee9dbd2a188839105073571bee1b1f', 'Editor', 'editor@localhost', 2),
(4, 'reviewer', '7ba917e4e5158c8a9ed6eda08a6ec572', 'Reviewer', 'reviewer@localhost', 3),
(6, 'penulis', 'de3709b8e6f81a4ef5a858b7a2d28883', 'Penulis', 'penulis@gmail.com', 4),
(7, 'pembaca', 'aa0a155fd35decc813f9251a36d1f81a', 'Pembaca', 'pembaca@gmail.com', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
