<?php
    if(isset($_GET['add'])){
        ?>
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Input Data Jurnal</h3>
          </div>
          <form role="form" method="post" action="">
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="username" placeholder="Masukan username">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Masukan password">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Lengkap</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="nama_pengguna" placeholder="Masukan nama lengkap">
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="btn_add">Tambah</button>
                <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
              </div>
            </div>
          </div>
            <!-- /.card-body -->
        <?php
        if(isset($_POST['btn_add'])){
            $ins = $db->pdo->prepare("insert into tbl_jurnal set username = '".$_POST['username']."',
                               password = '".md5($_POST['password'])."',
                               nama_pengguna = '".$_POST['nama_pengguna']."'");
            $ins->execute();
            echo "<script>location.href='./?page=daftar-jurnal'</script>";
        }
    }elseif(isset($_GET['edit'])){
        $e = $db->pdo->prepare("select * from tbl_jurnal where id_jurnal = '".$_GET['edit']."'");
        $e->execute();
        $re = $e->fetch();
        ?>
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Data Jurnal</h3>
          </div>
          <form role="form" method="post" action="">
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $re['username']; ?>" name="username" placeholder="Masukan username">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Masukan password"> *) kosongkan jika tidak diganti
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Lengkap</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $re['nama_pengguna']; ?>" name="nama_pengguna" placeholder="Masukan nama lengkap">
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="btn_add">Ubah</button>
                <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
              </div>
            </div>
          </div>
            <!-- /.card-body -->
        <?php
        if(isset($_POST['btn_add'])){
            $pass = empty($_POST['password']) ? "" : "password = '".md5($_POST['password'])."',";
            $ins = $db->pdo->prepare("update tbl_jurnal set username = '".$_POST['username']."',
                                      ".$pass." nama_pengguna = '".$_POST['nama_pengguna']."'
                                      where id_jurnal = '".$_GET['edit']."'");
            $ins->execute();
            echo "<script>location.href='./?page=daftar-jurnal'</script>";
        }
    }elseif(isset($_GET['hapus'])){
        $del = $db->pdo->prepare("delete from tbl_jurnal where id_jurnal = '".$_GET['hapus']."'");
        $del->execute();
        echo "<script>location.href='./?page=daftar-jurnal'</script>";
    }else{
      if(isset($_GET['conf'])){
          if($_GET['conf'] == 0){
              $up = $db->pdo->prepare("update tbl_jurnal set status_jurnal = '1' where id_jurnal = '".$_GET['id']."'");
              $up->execute();
              echo "<script>location.href='./?page=daftar-jurnal'</script>";
          }else{
              $up = $db->pdo->prepare("update tbl_jurnal set status_jurnal = '0' where id_jurnal = '".$_GET['id']."'");
              $up->execute();
              echo "<script>location.href='./?page=daftar-jurnal'</script>";
          }
      }elseif(isset($_GET['p'])){
              $del = $db->pdo->prepare("update tbl_jurnal set persetujuan = '".$_GET['p']."' where id_jurnal = '".$_GET['id']."'");
              $del->execute();
              echo "<script>location.href='./?page=daftar-jurnal'</script>";
      }elseif(isset($_GET['hapus'])){
          $up = $db->pdo->prepare("delete from tbl_jurnal where id_jurnal = '".$_GET['hapus']."'");
          $up->execute();
          echo "<script>location.href='./?page=daftar-jurnal'</script>";
      }
      ?>
      <script>
      function myFunction() {
        // Declare variables
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[1];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }
      </script>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Jurnal</h3>
          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" onkeyup="myFunction()" id="myInput" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover" id="myTable">
            <tr><th>No</th><th>Penulis</th><th>Status Terbit</th><th>Aksi</th></tr>
            <?php
                $l = $db->pdo->prepare("select * from tbl_jurnal, tbl_user
                                        where tbl_jurnal.id_user = tbl_user.id_user
                                        AND tbl_jurnal.status_terbit = '1' order by 1 asc");
                $l->execute();
                $no=1;
                while($rl = $l->fetch()){
                    $p = ($rl['persetujuan'] == 0) ? 1 : 0;
                    $persetujuan = ($rl['persetujuan'] == 0) ? "Belum disetujui" : "Disetujui";
                    echo "<tr><td>".$no."</td><td width=500>".$rl['nama_pengguna']."<br /><small>Jurnal : ".$rl['judul_jurnal']."</small>";
                    echo "<br /><small>Tanggal Unggah : ".$rl['tanggal_publikasi']."</small></td>";
                    echo "<td>".$persetujuan."</td>";
                    echo "<td><a href='../?page=ejurnal&id=".$rl['id_jurnal']."' target='_blank'>Lihat</a>";
                    if($rl['persetujuan'] == 0){
                        echo " | <a href='./?page=daftar-jurnal&id=".$rl['id_jurnal']."&p=".$p."' >Setujui</a>";
                    }else{
                        echo " | <a href='./?page=daftar-jurnal&id=".$rl['id_jurnal']."&p=".$p."' >Batal</a>";
                    }
                    if ($logged_user == 1) {
                      echo " | <a href='?page=daftar-jurnal&hapus=".$rl['id_jurnal']."'>Hapuss</a></td></tr>";
                    }
                    $no++;
                }
            ?>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <?php
    }
?>
