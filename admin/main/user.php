<?php
    if(isset($_GET['add'])){
        ?>
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Input Data User</h3>
          </div>
          <form role="form" method="post" action="">
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="username">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Pengguna</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="nama_pengguna">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Status</label>
                <select name="status" class="form-control">
                    <option selected></option>
                    <?php
                        foreach($user as $key=>$val){
                            echo "<option value='".$key."'>".$val."</option>";
                        }
                    ?> 
                </select>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="btn_add">Tambah</button>
                <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
              </div>
            </div>
          </div>
            <!-- /.card-body -->
        <?php
        if(isset($_POST['btn_add'])){
            $ins = $db->pdo->prepare("insert into tbl_user set username = '".$_POST['username']."',
                                       password = '".md5($_POST['password'])."',
                                       nama_pengguna = '".$_POST['nama_pengguna']."',
                                       email = '".$_POST['email']."', status = '".$_POST['status']."'");
            $ins->execute();
            echo "<script>location.href='./?page=user'</script>";
        }
    }elseif(isset($_GET['edit'])){
        $e = $db->pdo->prepare("select * from tbl_user where id_user = '".$_GET['edit']."'");
        $e->execute();
        $re = $e->fetch();
        ?>
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Data User</h3>
          </div>
          <form role="form" method="post" action="">
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $re['username']; ?>" name="username" placeholder="Masukan username">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Masukan password"> *) kosongkan jika tidak diganti
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Nama Pengguna</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $re['nama_pengguna']; ?>" name="nama_pengguna" placeholder="Masukan nama lengkap">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $re['email']; ?>" name="email">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Status</label>
                <select name="status" class="form-control">
                    <option selected></option>
                    <?php
                        foreach($user as $key=>$val){
                          if($key == $re['status']){
                            echo "<option value='".$key."' selected>".$val."</option>";
                          }else{
                            echo "<option value='".$key."'>".$val."</option>";
                          }
                        }
                    ?> 
                </select>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="btn_add">Ubah</button>
                <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
              </div>
            </div>
          </div>
            <!-- /.card-body -->
        <?php
        if(isset($_POST['btn_add'])){
            $pass = empty($_POST['password']) ? "" : "password = '".md5($_POST['password'])."',";
            $ins = $db->pdo->prepare("update tbl_user set username = '".$_POST['username']."',
                                      ".$pass." nama_pengguna = '".$_POST['nama_pengguna']."', email = '".$_POST['email']."',
                                      status = '".$_POST['status']."'
                                      where id_user = '".$_GET['edit']."'");
            $ins->execute();
            echo "<script>location.href='./?page=user'</script>";
        }
    }elseif(isset($_GET['hapus'])){
        $del = $db->pdo->prepare("delete from tbl_user where id_user = '".$_GET['hapus']."'");
        $del->execute();
        echo "<script>location.href='./?page=user'</script>";
    }else{
      ?>
      <script>
      function myFunction() {
        // Declare variables
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[1];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }
      </script>
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data User | <a href="?page=user&add">Tambah User</a></h3>
          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" onkeyup="myFunction()" id="myInput" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover" id="myTable">
            <tr><th>No</th><th>Username</th><th>Nama Pengguna</th><th>Email</th><th>Status</th><th>Aksi</th></tr>
            <?php
                $l = $db->pdo->prepare("select * from tbl_user order by 1 asc");
                $l->execute();
                $no=1;
                while($rl = $l->fetch()){
                    echo "<tr><td>".$no."</td><td>".$rl['username']."</td><td>".$rl['nama_pengguna']."</td>";
                    echo "<td>".$rl['email']."</td><td>".user($rl['status'])."</td>";
                    echo "<td><a href='?page=user&edit=".$rl['id_user']."'>Edit</a> | <a href='?page=user&hapus=".$rl['id_user']."'>Hapus</a></td></tr>";
                    $no++;
                }
            ?>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <?php
    }
?>
