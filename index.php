<?php
    session_start();
    include "./config/db.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SI e-Jurnal Pengabdian Kepada Masyarakat</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.html"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Beranda</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./?page=tentang">Tentang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./?page=arsip">Arsip</a>
            </li>
            <?php
                if(empty($_SESSION['uid'])){
                    ?>
                    <li class="nav-item">
                      <a class="nav-link" href="./?page=login">Masuk</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="./?page=register">Pendaftaran</a>
                    </li>
                    <?php
                }elseif($logged_user == 2){
                    ?>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Panel Editor
                      </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="./?page=profil">Profil Saya</a>
                        <a class="dropdown-item" href="./?page=daftar-jurnal">Daftar Jurnal</a>
                      </div>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="./?page=logout" onclick="return confirm('Apakah anda yakin?')">Logout</a>
                    </li>
                    <?php
                }elseif($logged_user == 3){
                    ?>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Panel Reviewer
                      </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="./?page=profil">Profil Saya</a>
                        <a class="dropdown-item" href="./?page=daftar-jurnal">Daftar Jurnal</a>
                      </div>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="./?page=logout" onclick="return confirm('Apakah anda yakin?')">Logout</a>
                    </li>
                    <?php
                }elseif($logged_user == 4){
                    ?>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Panel Penulis
                      </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="./?page=profil">Profil Saya</a>
                        <a class="dropdown-item" href="./?page=daftar-jurnal">Daftar Jurnal</a>
                        <a class="dropdown-item" href="./?page=daftar-jurnal&add">Tambah Jurnal</a>
                      </div>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="./?page=logout" onclick="return confirm('Apakah anda yakin?')">Logout</a>
                    </li>
                    <?php
                }else{
                    ?>
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Panel Pembaca
                      </a>
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="./?page=profil">Profil Saya</a>
                      </div>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="./?page=logout" onclick="return confirm('Apakah anda yakin?')">Logout</a>
                    </li>
                    <?php
                }
            ?>
            <!--<li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Portfolio
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                <a class="dropdown-item" href="portfolio-1-col.html">1 Column Portfolio</a>
                <a class="dropdown-item" href="portfolio-2-col.html">2 Column Portfolio</a>
                <a class="dropdown-item" href="portfolio-3-col.html">3 Column Portfolio</a>
                <a class="dropdown-item" href="portfolio-4-col.html">4 Column Portfolio</a>
                <a class="dropdown-item" href="portfolio-item.html">Single Portfolio Item</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Blog
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                <a class="dropdown-item" href="blog-home-1.html">Blog Home 1</a>
                <a class="dropdown-item" href="blog-home-2.html">Blog Home 2</a>
                <a class="dropdown-item" href="blog-post.html">Blog Post</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Other Pages
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                <a class="dropdown-item" href="full-width.html">Full Width Page</a>
                <a class="dropdown-item" href="sidebar.html">Sidebar Page</a>
                <a class="dropdown-item" href="faq.html">FAQ</a>
                <a class="dropdown-item" href="404.html">404</a>
                <a class="dropdown-item" href="pricing.html">Pricing Table</a>
              </div>
            </li>-->
          </ul>
        </div>
      </div>
    </nav>

    

    <!-- Page Content -->
    <div class="container">
        <?php
            if(isset($_GET['page']) && ($_GET['page'] == 'register')){
                include "./main/register.php";
            }elseif(isset($_GET['page']) && ($_GET['page'] == 'login')){
                include "./main/login.php";
            }elseif(isset($_GET['page']) && ($_GET['page'] == 'profil')){
                include "./main/profil.php";
            }elseif(isset($_GET['page']) && ($_GET['page'] == 'daftar-jurnal')){
                if($logged_user == 4){
                  include "./main/jurnal.php";
                }elseif($logged_user == 2){
                  include "./main/jurnal-editor.php";
                }else{
                  include "./main/jurnal-reviewer.php";
                }
            }elseif(isset($_GET['page']) && ($_GET['page'] == 'arsip')){
                include "./main/arsip.php";
            }elseif(isset($_GET['page']) && ($_GET['page'] == 'tentang')){
                include "./main/tentang.php";
            }elseif(isset($_GET['page']) && ($_GET['page'] == 'ejurnal')){
                include "./main/ejurnal.php";
            }elseif(isset($_GET['page']) && ($_GET['page'] == 'logout')){
                unset($_SESSION['uid']);
                echo "<script>location.href='./index.php'</script>";
            }else{
                include "./welcome.php";
            }
        ?>
    <!-- /.container -->
  </div>
    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; SI e-Jurnal Pengabdian Kepada Masyarakat</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
