<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Registrasi User Terdaftar</h3>
  </div>
  <form role="form" method="post" action="">
    <div class="card-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Username</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="username" placeholder="Masukan username">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Masukan password">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Pengguna</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="nama_pengguna" placeholder="Masukan nama lengkap">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">E-mail</label>
        <input type="text" class="form-control" id="exampleInputEmail1" name="email" placeholder="Masukan e-mail">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Daftar Sebagai</label>
        <select name="status" class="form-control">
            <option selected></option>
            <?php
                foreach($user as $key=>$val){
                  if($key !== 1 && $key !== 2 && $key !== 3){
                    if($key == $re['status']){
                      echo "<option value='".$key."' selected>".$val."</option>";
                    }else{
                      echo "<option value='".$key."'>".$val."</option>";
                    }
                  }
                }
            ?> 
        </select>
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary" name="btn_add">Daftar</button>
        <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
      </div>
    </div>
    <?php
    	if(isset($_POST['btn_add'])){
    		$ins = $db->pdo->prepare("insert into tbl_user set username = '".$_POST['username']."',
                								  password = '".md5($_POST['password'])."', nama_pengguna = '".$_POST['nama_pengguna']."',
                                  email = '".$_POST['email']."', status = '".$_POST['status']."'");
    		$ins->execute();
    		echo "<script>alert('Terima kasih telah registrasi, silahkan login!')</script>";
    		echo "<script>location.href='./?page=login'</script>";
		}
    ?>
  </div>