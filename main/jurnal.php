<?php
    if(isset($_GET['add'])){
        ?>
        <!-- Page Heading/Breadcrumbs --><br >

            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="index.php">Beranda</a>
              </li>
              <li class="breadcrumb-item active">Tambah Jurnal</li>
            </ol>

            <div class="card card-primary">
        <?php
            $p = $db->pdo->prepare("select * from tbl_penulis where id_penulis = '".$_SESSION['uid']."'");
            $p->execute();
            $rp = $p->fetch();
        ?>
        <script>
        function showHint(str) {
            if (str.length == 0) {
                document.getElementById("txtHint").innerHTML = "";
                return;
            } else {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("txtHint").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("GET", "/e-journal/main/gethint.php?q=" + str, true);
                xmlhttp.send();
            }
        }
        </script>
        <form role="form" method="post" action="" enctype="multipart/form-data">
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Judul Jurnal</label>
              <input type="text" class="form-control" id="exampleInputEmail1" required="true" name="judul_jurnal" placeholder="Masukan judul jurnal" autocomplete="off" onkeyup="showHint(this.value)">
              <p id="txtHint" class="text-danger"></p>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Abstrak</label>
              <textarea name="abstrak" required="true" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Deskripsi</label>
              <textarea name="deskripsi" required="true" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">File</label>
              <input type="file" name="file" required="true" class="form-control">
            </div>
            <?php
              if ($logged_user == 1) {
            ?>
              <div class="form-group">
                <label for="exampleInputPassword1">Gambar</label>
                <input type="file" name="gambar" required="true" class="form-control">
              </div>
            <?php } ?>
            <div class="form-group">
              <label for="exampleInputEmail1">Kata Kunci</label>
              <input type="text" class="form-control" required="true" id="exampleInputEmail1" name="kata_kunci" placeholder="Masukan judul jurnal">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Referensi</label>
              <textarea name="referensi" required="true" class="form-control"></textarea>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" name="btn_add">Tambah</button>
              <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
            </div>
          </div>
          <?php
              $filename = $_FILES['file']['name'];
              $filesize = $_FILES['file']['size'];
              $gambar = $_FILES['gambar']['name'];
              if(isset($_POST['btn_add'])){
                  $c = $db->pdo->prepare("select * from tbl_jurnal where judul_jurnal LIKE '%".$_POST['judul_jurnal']."%'");
                  $c->execute();
                  if($c->rowCount()>0){
                      echo '<script>alert("Judul yang anda inputkan sudah tersedia!");</script>';
                  }elseif($filesize > 51200000){
                      echo "<script>alert('Ukuran File Terlalu Besar!')</script>";
                  }else{
                    $temp = explode(".", $filename);
                    $newfilename = round(microtime(true)) . '.' . end($temp);
                    move_uploaded_file($_FILES['file']['tmp_name'], './file/'.$newfilename);

                    $tempx = explode(".", $gambar);
                    $newfilenamex = round(microtime(true)) . '.' . end($tempx);
                    move_uploaded_file($_FILES['gambar']['tmp_name'], './gambar/'.$newfilenamex);
                    $up = $db->pdo->prepare("insert into tbl_jurnal set judul_jurnal = '".$_POST['judul_jurnal']."',
                                             id_user = '".$_SESSION['uid']."', abstrak = '".$_POST['abstrak']."',
                                             deskripsi = '".$_POST['deskripsi']."', file = '".$newfilename."',
                                             gambar = '".$newfilenamex."',
                                             kata_kunci = '".$_POST['kata_kunci']."', referensi = '".$_POST['referensi']."',
                                             tanggal_publikasi = '".date("Y-m-d")."', tahun = '".date("Y")."'");
                    $up->execute();
                    echo "<script>location.href='./?page=daftar-jurnal'</script>";
                }
              }
          ?>
          </div>
        <?php
    }else{
        if(isset($_GET['hapus'])){
            $d = $db->pdo->prepare("delete from tbl_jurnal where id_jurnal = '".$_GET['hapus']."'");
            $d->execute();
            echo "<script>location.href='./?page=daftar-jurnal'</script>";
        }elseif(isset($_GET['lihat-rev'])){
            $j = $db->pdo->prepare("select * from tbl_jurnal where id_jurnal = '".$_GET['lihat-rev']."'");
            $j->execute();
            $rj = $j->fetch();
            ?>
            <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Revisi</h3>
              <div class="card-tools">
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover" id="myTable">
                <tr><th colspan="100"><small><?php echo $rj['judul_jurnal']; ?></small></th></tr>
                <tr><th>Revisi Ke</th><th>Revisi</th><th>Dari</th><th>Tanggal Revisi</th></tr>
                <?php
                    $l = $db->pdo->prepare("select * from tbl_revisi where id_jurnal = '".$_GET['lihat-rev']."' order by 1 asc");
                    $l->execute();
                    $no=1;
                    while($rl = $l->fetch()){
                        echo "<tr><td>".$no."</td><td>".$rl['revisi']."</td>";
                        echo "<td>".getuser($rl['id_user'])."</td>";
                        echo "<td>".$rl['tanggal_revisi']."</td></tr>";
                        $no++;
                    }
                ?>
              </table>
            </div>
          </div>
            <?php
        }elseif(isset($_GET['edit'])){
            $e = $db->pdo->prepare("select * from tbl_jurnal where id_jurnal = '".$_GET['edit']."'");
            $e->execute();
            $re = $e->fetch();
            ?>
            <h1 class="mt-4 mb-3">Edit
              <small>Jurnal</small>
            </h1>

              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="index.php">Beranda</a>
                </li>
                <li class="breadcrumb-item active">Edit Jurnal</li>
              </ol>

              <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Penulis</h3>
          </div>
          <?php
              $p = $db->pdo->prepare("select * from tbl_penulis where id_penulis = '".$_SESSION['uid']."'");
              $p->execute();
              $rp = $p->fetch();
          ?>
          <form role="form" method="post" action="" enctype="multipart/form-data">
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Judul Jurnal</label>
                <input type="text" class="form-control" required="true" id="exampleInputEmail1" value="<?php echo $re['judul_jurnal']; ?>" name="judul_jurnal" placeholder="Masukan judul jurnal">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Abstrak</label>
                <textarea name="abstrak" required="true" class="form-control"><?php echo $re['abstrak']; ?></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Deskripsi</label>
                <textarea name="deskripsi" required="true" class="form-control"><?php echo $re['deskripsi']; ?></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">File (* Kosongkan jika tidak diganti)</label>
                <input type="file" name="file" class="form-control">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Gambar (* Kosongkan jika tidak diganti)</label>
                <input type="file" name="gambar" class="form-control">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Kata Kunci</label>
                <input type="text" required="true" class="form-control" id="exampleInputEmail1" value="<?php echo $re['kata_kunci']; ?>" name="kata_kunci" placeholder="Masukan judul jurnal">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Referensi</label>
                <textarea name="referensi" required="true" class="form-control"><?php echo $re['referensi']; ?></textarea>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="btn_add">Ubah</button>
                <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
              </div>
            </div>
            <?php
                $filename = $_FILES['file']['name'];
                $gambar = $_FILES['gambar']['name'];
                if(isset($_POST['btn_add'])){
                    if(empty($filename)){
                          $up = $db->pdo->prepare("update tbl_jurnal set judul_jurnal = '".$_POST['judul_jurnal']."',
                                                   id_penulis = '".$_SESSION['uid']."', abstrak = '".$_POST['abstrak']."',
                                                   deskripsi = '".$_POST['deskripsi']."',
                                                   kata_kunci = '".$_POST['kata_kunci']."', referensi = '".$_POST['referensi']."',
                                                   tanggal_publikasi = '".date("Y-m-d")."'
                                                   where id_jurnal = '".$_GET['edit']."'");
                          $up->execute();
                          echo "<script>location.href='./?page=daftar-jurnal'</script>";
                    }else{
                          $temp = explode(".", $filename);
                          $newfilename = round(microtime(true)) . '.' . end($temp);
                          move_uploaded_file($_FILES['file']['tmp_name'], './file/'.$newfilename);
                          $up = $db->pdo->prepare("update tbl_jurnal set judul_jurnal = '".$_POST['judul_jurnal']."',
                                                   id_penulis = '".$_SESSION['uid']."', abstrak = '".$_POST['abstrak']."',
                                                   deskripsi = '".$_POST['deskripsi']."', file = '".$newfilename."',
                                                   kata_kunci = '".$_POST['kata_kunci']."', referensi = '".$_POST['referensi']."',
                                                   tanggal_publikasi = '".date("Y-m-d")."'
                                                   where id_jurnal = '".$_GET['edit']."'");
                          $up->execute();
                          echo "<script>location.href='./?page=daftar-jurnal'</script>";
                    }
                }
            ?>
            </div>
            <?php
        }else{
        ?>
        <!-- Page Heading/Breadcrumbs --><br />

            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="index.php">Beranda</a>
              </li>
              <li class="breadcrumb-item active">Daftar Jurnal</li>
            </ol>

            <div class="card card-primary">
              <script>
      function myFunction() {
        // Declare variables
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[1];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }
      </script>
      <div class="card">
        <div class="card-header">
          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" onkeyup="myFunction()" id="myInput" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover" id="myTable">
            <tr><th>No</th><th>Judul</th><th>Status Revisi</th><th>Revisi</th><th>Status Terbit</th><th>Aksi</th></tr>
            <?php
                $l = $db->pdo->prepare("select * from tbl_jurnal where id_user = '".$_SESSION['uid']."' order by 1 asc");
                $l->execute();
                $no=1;
                while($rl = $l->fetch()){
                    $stat = ($rl['status_revisi'] == 0 ? "Belum dikonfirmasi" : "Dikonfirmasi");
                    $statx = ($rl['persetujuan'] == 0 ? "Belum Diterbitkan" : "Diterbitkan");
                    echo "<tr><td>".$no."</td><td>".$rl['judul_jurnal']."<br>";
                    echo "<small>Tanggal Unggah : ".$rl['tanggal_publikasi']."</small></td>";
                    echo "<td>".$stat."</td><td>";
                    $r = $db->pdo->prepare("select * from tbl_revisi where id_jurnal = '".$rl['id_jurnal']."'");
                    $r->execute();
                    echo $r->rowCount();
                    echo " (<a href='?page=daftar-jurnal&lihat-rev=".$rl['id_jurnal']."'>Lihat</a>)</td>";
                    echo "<td>".$statx."</td><td><a href='?page=ejurnal&id=".$rl['id_jurnal']."' target='_blank'>Lihat</a> | <a href='?page=daftar-jurnal&edit=".$rl['id_jurnal']."'>Edit</a>";
                    if ($logged_user == 1) {
                      echo "| <a href='?page=daftar-jurnal&hapus=".$rl['id_jurnal']."'>Hapus</a></td></tr>";
                    }
                    $no++;
                }
            ?>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
          </div>
        <?php
      }
    }
?>
