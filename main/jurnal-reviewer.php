<?php
    if(isset($_GET['add'])){
        ?>
        <!-- Page Heading/Breadcrumbs -->
            <h1 class="mt-4 mb-3">Tambah
              <small>Jurnal</small>
            </h1>

            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="index.php">Beranda</a>
              </li>
              <li class="breadcrumb-item active">Tambah Jurnal</li>
            </ol>

            <div class="card card-primary">
        <?php
            $p = $db->pdo->prepare("select * from tbl_penulis where id_penulis = '".$_SESSION['uid']."'");
            $p->execute();
            $rp = $p->fetch();
        ?>  
        <form role="form" method="post" action="" enctype="multipart/form-data">
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Judul Jurnal</label>
              <input type="text" class="form-control" id="exampleInputEmail1" name="judul_jurnal" placeholder="Masukan judul jurnal">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Abstrak</label>
              <textarea name="abstrak" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Deskripsi</label>
              <textarea name="deskripsi" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">File</label>
              <input type="file" name="file" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Kata Kunci</label>
              <input type="text" class="form-control" id="exampleInputEmail1" name="kata_kunci" placeholder="Masukan judul jurnal">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Referensi</label>
              <textarea name="referensi" class="form-control"></textarea>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" name="btn_add">Tambah</button>
              <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
            </div>
          </div>
          <?php
              $filename = $_FILES['file']['name'];
              if(isset($_POST['btn_add'])){
                  move_uploaded_file($_FILES['file']['tmp_name'], './file/'.$filename);
                  $up = $db->pdo->prepare("insert into tbl_jurnal set judul_jurnal = '".$_POST['judul_jurnal']."',
                                           id_user = '".$_SESSION['uid']."', abstrak = '".$_POST['abstrak']."',
                                           deskripsi = '".$_POST['deskripsi']."', file = '".$filename."',
                                           kata_kunci = '".$_POST['kata_kunci']."', referensi = '".$_POST['referensi']."',
                                           tanggal_publikasi = '".date("Y-m-d")."', tahun = '".date("Y")."'");
                  $up->execute();
                  echo "<script>location.href='./?page=daftar-jurnal'</script>";
              }
          ?>
          </div>
        <?php
    }else{
        if(isset($_GET['hapus'])){
            $d = $db->pdo->prepare("delete from tbl_jurnal where id_jurnal = '".$_GET['hapus']."'");
            $d->execute();
            echo "<script>location.href='./?page=daftar-jurnal'</script>";
        }elseif(isset($_GET['revisi'])){
            $r = $db->pdo->prepare("select * from tbl_jurnal where id_jurnal = '".$_GET['revisi']."'");
            $r->execute();
            $rr = $r->fetch();
            ?>
            <h1 class="mt-4 mb-3">Revisi
              <small>Jurnal</small>
            </h1>

            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="index.php">Beranda</a>
              </li>
              <li class="breadcrumb-item active">Revisi</li>
            </ol>

            <div class="card card-primary">
        <form role="form" method="post" action="" enctype="multipart/form-data">
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Judul Jurnal</label>
              <input type="text" class="form-control" id="exampleInputEmail1" name="judul_jurnal" value="<?php echo $rr['judul_jurnal']; ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Revisi</label>
              <textarea name="revisi" class="form-control"></textarea>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" name="btn_add">Input Revisi</button>
              <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
            </div>
          </div>
        </div>
          <?php
              if(isset($_POST['btn_add'])){
                  $up = $db->pdo->prepare("insert into tbl_revisi set id_jurnal = '".$_GET['revisi']."',
                                           id_user = '".$_SESSION['uid']."', revisi = '".$_POST['revisi']."',
                                           tanggal_revisi = '".date("Y-m-d")."'");
                  $up->execute();
                  echo "<script>location.href='./?page=daftar-jurnal'</script>";
              }
        }elseif(isset($_GET['lihat-rev'])){
            if(isset($_GET['edit'])){
              $r = $db->pdo->prepare("select * from tbl_revisi, tbl_jurnal where tbl_revisi.id_jurnal = tbl_jurnal.id_jurnal
                                      AND tbl_revisi.id_revisi = '".$_GET['edit']."'");
                $r->execute();
                $rr = $r->fetch();
                ?>
                <h1 class="mt-4 mb-3">Revisi
                  <small>Jurnal</small>
                </h1>

                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="index.php">Beranda</a>
                  </li>
                  <li class="breadcrumb-item active">Revisi</li>
                </ol>

                <div class="card card-primary">
            <form role="form" method="post" action="" enctype="multipart/form-data">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Judul Jurnal</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" name="judul_jurnal" value="<?php echo $rr['judul_jurnal']; ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Revisi</label>
                  <textarea name="revisi" class="form-control"><?php echo $rr['revisi']; ?></textarea>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="btn_add">Ubah Revisi</button>
                  <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
                </div>
              </div>
            </div>
              <?php
                  if(isset($_POST['btn_add'])){
                      $up = $db->pdo->prepare("update tbl_revisi set revisi = '".$_POST['revisi']."'
                                              where id_revisi = '".$_GET['edit']."'");
                      $up->execute();
                      echo "<script>location.href='./?page=daftar-jurnal&lihat-rev=".$_GET['lihat-rev']."'</script>";
                  }
            }elseif(isset($_GET['hapux'])){
                $d = $db->pdo->prepare("delete from tbl_revisi where id_revisi = '".$_GET['hapux']."'");
                $d->execute();
                echo "<script>location.href='?page=daftar-jurnal&lihat-rev=".$_GET['lihat-rev']."'</script>";
            }else{
              $j = $db->pdo->prepare("select * from tbl_jurnal where id_jurnal = '".$_GET['lihat-rev']."'");
              $j->execute();
              $rj = $j->fetch();
              ?>
              <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Revisi</h3>
                <div class="card-tools">
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover" id="myTable">
                  <tr><th colspan="100"><small><?php echo $rj['judul_jurnal']; ?></small></th></tr>
                  <tr><th>Revisi Ke</th><th>Revisi</th><th>Dari</th><th>Tanggal Revisi</th><th>Aksi</th></tr>
                  <?php
                      $l = $db->pdo->prepare("select * from tbl_revisi where id_jurnal = '".$_GET['lihat-rev']."' order by 1 asc");
                      $l->execute();
                      $no=1;
                      while($rl = $l->fetch()){
                          echo "<tr><td>".$no."</td><td>".$rl['revisi']."</td>";
                          echo "<td>".getuser($rl['id_user'])."</td>";
                          echo "<td>".$rl['tanggal_revisi']."</td>";
                          echo "<td><a href='?page=daftar-jurnal&lihat-rev=".$_GET['lihat-rev']."&edit=".$rl['id_revisi']."'>Edit</a>";
                          echo " | <a href='?page=daftar-jurnal&lihat-rev=".$_GET['lihat-rev']."&hapux=".$rl['id_revisi']."'>Hapus</a></td></tr>";
                          $no++;
                      }
                  ?>
                </table>
              </div>
            </div>
              <?php
          }
        }elseif(isset($_GET['edit'])){
            $e = $db->pdo->prepare("select * from tbl_jurnal where id_jurnal = '".$_GET['edit']."'");
            $e->execute();
            $re = $e->fetch();
            ?>
            <h1 class="mt-4 mb-3">Edit
              <small>Jurnal</small>
            </h1>

              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="index.php">Beranda</a>
                </li>
                <li class="breadcrumb-item active">Edit Jurnal</li>
              </ol>

              <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Reviewer</h3>
          </div>
          <?php
              $p = $db->pdo->prepare("select * from tbl_penulis where id_penulis = '".$_SESSION['uid']."'");
              $p->execute();
              $rp = $p->fetch();
          ?>  
          <form role="form" method="post" action="" enctype="multipart/form-data">
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Judul Jurnal</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $re['judul_jurnal']; ?>" name="judul_jurnal" placeholder="Masukan judul jurnal">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Abstrak</label>
                <textarea name="abstrak" class="form-control"><?php echo $re['abstrak']; ?></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Deskripsi</label>
                <textarea name="deskripsi" class="form-control"><?php echo $re['deskripsi']; ?></textarea>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">File (* Kosongkan jika tidak diganti)</label>
                <input type="file" name="file" class="form-control">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Kata Kunci</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="<?php echo $re['kata_kunci']; ?>" name="kata_kunci" placeholder="Masukan judul jurnal">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Referensi</label>
                <textarea name="referensi" class="form-control"><?php echo $re['referensi']; ?></textarea>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="btn_add">Ubah</button>
                <button type="button" class="btn btn-primary" onclick="self.history.back()">Batal</button>
              </div>
            </div>
            <?php
                $filename = $_FILES['file']['name'];
                if(isset($_POST['btn_add'])){
                    if(empty($filename)){
                          $up = $db->pdo->prepare("update tbl_jurnal set judul_jurnal = '".$_POST['judul_jurnal']."',
                                                   id_penulis = '".$_SESSION['uid']."', abstrak = '".$_POST['abstrak']."',
                                                   deskripsi = '".$_POST['deskripsi']."', 
                                                   kata_kunci = '".$_POST['kata_kunci']."', referensi = '".$_POST['referensi']."',
                                                   tanggal_publikasi = '".date("Y-m-d")."'
                                                   where id_jurnal = '".$_GET['edit']."'");
                          $up->execute();
                          echo "<script>location.href='./?page=daftar-jurnal'</script>";
                    }else{
                          move_uploaded_file($_FILES['file']['tmp_name'], './file/'.$filename);
                          $up = $db->pdo->prepare("update tbl_jurnal set judul_jurnal = '".$_POST['judul_jurnal']."',
                                                   id_penulis = '".$_SESSION['uid']."', abstrak = '".$_POST['abstrak']."',
                                                   deskripsi = '".$_POST['deskripsi']."', file = '".$filename."',
                                                   kata_kunci = '".$_POST['kata_kunci']."', referensi = '".$_POST['referensi']."',
                                                   tanggal_publikasi = '".date("Y-m-d")."'
                                                   where id_jurnal = '".$_GET['edit']."'");
                          $up->execute();
                          echo "<script>location.href='./?page=daftar-jurnal'</script>";
                    }
                }
            ?>
            </div>
            <?php
        }elseif(isset($_GET['conf'])){
            $u = $_GET['conf'] == 0 ? 1 : 0;
            $up = $db->pdo->prepare("update tbl_jurnal set persetujuan = '".$u."' WHERE id_jurnal = '".$_GET['id']."'");
            $up->execute();
            echo "<script>location.href='?page=daftar-jurnal'</script>";
        }else{
        ?>
        <!-- Page Heading/Breadcrumbs --><br />

            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="index.php">Beranda</a>
              </li>
              <li class="breadcrumb-item active">Daftar Jurnal</li>
            </ol>

            <div class="card card-primary">
              <script>
      function myFunction() {
        // Declare variables
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[1];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }
      </script>
      <div class="card">
        <div class="card-header">
          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" onkeyup="myFunction()" id="myInput" class="form-control float-right" placeholder="Search">

              <div class="input-group-append">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover" id="myTable">
            <tr><th>No</th><th>Judul</th><th>Status Revisi</th><th>Status Terbit</th><th>Revisi</th><th>Aksi</th></tr>
            <?php
                $l = $db->pdo->prepare("select * from tbl_jurnal, tbl_user
                                        where tbl_jurnal.id_user = tbl_user.id_user order by 1 asc");
                $l->execute();
                $no=1;
                while($rl = $l->fetch()){
                    $stat = ($rl['status_revisi'] == 0 ? "Belum dikonfirmasi" : "Dikonfirmasi");
                    $statx = ($rl['persetujuan'] == 0 ? "Belum diterbitkan" : "Diterbitkan");
                    $statxx = ($rl['persetujuan'] == 0 ? "Setujui Terbit" : "Batal Terbit");
                    echo "<tr><td>".$no."</td><td>".$rl['judul_jurnal']."<br>";
                    echo "<small>".$rl['nama_pengguna'].", Tanggal Unggah : ".$rl['tanggal_publikasi']."</small></td>";
                    echo "<td>".$stat."</td><td>".$statx."</td>";
                    echo '<td>';
                    $r = $db->pdo->prepare("select * from tbl_revisi where id_jurnal = '".$rl['id_jurnal']."'");
                    $r->execute();
                    echo $r->rowCount();
                    echo ' (<a href="?page=daftar-jurnal&lihat-rev='.$rl['id_jurnal'].'">Lihat</a>)</td>';
                    echo "<td><a href='?page=ejurnal&id=".$rl['id_jurnal']."' target='_blank'>Lihat</a>";
                    echo " | <a href='?page=daftar-jurnal&revisi=".$rl['id_jurnal']."'>Revisi</a> | ";
                    echo "<a href='?page=daftar-jurnal&conf=".$rl['persetujuan']."&id=".$rl['id_jurnal']."'>".$statxx."</a></td></tr>";
                    echo "</td></tr>";
                    $no++;
                }
            ?>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
          </div>
        <?php
      }
    }
?>