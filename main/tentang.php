<div class="card card-primary">
<div class="card-header">
  <h3 class="card-title">Tentang</h3>
</div>
<center><h3 style='padding:10px;'>Visi Misi & Tujuan</h3></center>
<p style='text-align:justify; padding:10px;'>

Visi 
pada tahun 2020 menjadi program studi yang mampu bersaing dalam tataran global serta menghasilkan lulusan yang berkompeten dan profesional dibidang teknologi informasi khususnya rekayasa perangkat lunak dan sistem informasi serta menjadi pusat keunggulan dibidang pendidikan,
penelitian dan pengembangan ilmu pengetahuan dan teknologi dibidang Teknologi Informasi.

Berkompeten, mengandung arti bahwa lulusan Program Studi Teknik Informatika Universitas Nurtanio Bandung memiliki kemampuan atau kecakapan yang mencakup pengetahuan, keterampilan dan sikap sesuai dengan standar yang ditentukan oleh universitas

Profesional, mengandung arti bahwa lulusan Program Studi Teknik Informatika memiliki kemampuan dalam mencapai standar yang sudah ditentukan oleh program studi.
</div>