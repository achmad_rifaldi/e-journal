<?php
	if(isset($_GET['id'])){
		?>
		<div class="card card-primary">
		<div class="card-header">
		  <h3 class="card-title">Detail <small>Jurnal</small></h3>
		</div>
		<?php
		$j = $db->pdo->prepare("select * from tbl_jurnal, tbl_user where tbl_jurnal.id_user = tbl_user.id_user AND tbl_jurnal.id_jurnal = '".$_GET['id']."'");
		$j->execute();
		$rj = $j->fetch();
		?>
		<center><h4 style='padding:10px;'><?php echo $rj['judul_jurnal']; ?></h4></center>
		<center><small><?php echo $rj['nama_lengkap']; ?></small></center>
		<center><small><?php echo $rj['tanggal_publikasi']; ?></small></center>
		<center><small>Status Jurnal : <?php echo statust($rj['persetujuan']); ?></small></center>
		<p style='text-align:justify; padding:10px;'>
		<strong>Gambar</strong><br />
		<img src="./gambar/<?php echo $rj['gambar']; ?>" width="300px;">
		<br /><br />
		<strong>Abstrak</strong><br />
		<?php echo $rj['abstrak']; ?>
		<br /><br />

		<strong>Deskripsi</strong><br />
		<?php echo $rj['deskripsi']; ?>
		<br /><br />

		<strong>Kata Kunci</strong><br />
		<?php echo $rj['kata_kunci']; ?>
		<br /><br />

		<strong>Referensi</strong><br />
		<?php echo $rj['referensi']; ?>
		<br /><br />

		<strong>Full Text :</strong><br />
		<?php
			if(empty($_SESSION['uid']) && empty($_SESSION['authid'])){
				?><a href='?page=login'>Login untuk mendownload</a><?php
			}else{
				?><a href='?page=ejurnal&view=<?php echo $_GET['id']; ?>'>PDF (Bahasa Indonesia)</a><?php
			}
		?>
		</div>
		<?php
	}else{
		?>
		<div class="card card-primary">
		<div class="card-header">
		  <h3 class="card-title">Full PDF <small>Jurnal</small></h3>
		</div>
		<?php
		$j = $db->pdo->prepare("select * from tbl_jurnal, tbl_user where tbl_jurnal.id_user = tbl_user.id_user AND tbl_jurnal.id_jurnal = '".$_GET['view']."'");
		$j->execute();
		$rj = $j->fetch();
		?>
		<br /><br />
		<embed src="file/<?php echo $rj['file']; ?>" type="application/pdf" width="100%" height="700px">
		</div>
		<?php
	}
?>	