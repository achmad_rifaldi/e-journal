<?php
define("_NILAIK_", 4);
Class db {
	public $pdo;
	public function connect(){
		try {
      $db = new PDO("mysql:host=localhost;dbname=app_novika","root","");
		  if($db===FALSE){
				throw new Exception("Connection Failed");
			}else{
				$this -> pdo = $db;
			}
		} catch(Exception $e){
			echo "Error : ".$e -> getMessage();
		}
		return $this -> pdo;
	}
}

$db = new db;
$db->connect();

function arsip(){
	global $db;
	$j = $db->pdo->prepare("select * from tbl_jurnal where persetujuan = '1' group by tahun order by tahun");
	$j->execute();

	$tahun = [];
	$y = [];

	while($rj = $j->fetch()){
		$tahun[] = $rj['tahun'];
	}

	for($i=0; $i<count($tahun); $i++){
		$j = $db->pdo->prepare("select * from tbl_jurnal where tahun = '".$tahun[$i]."' AND persetujuan = '1' order by 1 asc");
		$j->execute();
		$id_jurnal = NULL;
		while($rj = $j->fetch()){
			$id_jurnal[] = $rj['id_jurnal'];
		}
		$vol = ($i+1);
		$id_jurnal = array_chunk($id_jurnal, 7);
		$y[$tahun[$i]] = array($i=>array_chunk($id_jurnal,2));
	}

	return $y;
}

$user = array(1=>"Admin", 2=>"Editor", 3=>"Reviewer", 4=>"Penulis", 5=>"Pembaca");

function user($i){
	global $user;
	return $user[$i];
}

function statusj($i){
	$s = $i==0 ? "Belum dikonfirmasi" : "Dikonfirmasi";
	return $s;
}

function statust($i){
	$s = $i==0 ? "Belum Diterbitkan" : "Diterbitkan";
	return $s;
}

function getuser($id){
	global $db;
	$u = $db->pdo->prepare("select * from tbl_user where id_user = '".$id."'");
	$u->execute();
	$ru = $u->fetch();
	return $ru['nama_pengguna']." (".user($ru['status']).")";
}

if ($_SESSION) {
	$sess = $db->pdo->prepare("select * from tbl_user where id_user = '".$_SESSION['uid']."'");
	$sess->execute();
	$rsess = $sess->fetch();
	$logged_user = $rsess['status'];
}
?>
